// harvester
Game.spawns.Shadowhall.createCreep([WORK,WORK,WORK,WORK,CARRY,CARRY,MOVE], undefined, { role: 'harvester', working: false, burndown: false });
// miner
Game.spawns.Shadowhall.createCreep([WORK,WORK,WORK,WORK,CARRY,CARRY,MOVE], undefined, { role: 'miner', working: false, burndown: false });
// upgrader
Game.spawns.Shadowhall.createCreep([WORK,WORK,WORK,CARRY,CARRY,CARRY,MOVE,MOVE], undefined, { role: 'upgrader', working: false, burndown: false });
// handyman
Game.spawns.Shadowhall.createCreep([WORK,WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE], undefined, { role: 'handyman', working: false, burndown: false });
// builder
Game.spawns.Shadowhall.createCreep([WORK,WORK,WORK,CARRY,CARRY,CARRY,MOVE,MOVE], undefined, { role: 'builder', working: false, burndown: false }); 